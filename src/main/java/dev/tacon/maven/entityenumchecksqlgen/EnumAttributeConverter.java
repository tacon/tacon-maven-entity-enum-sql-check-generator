package dev.tacon.maven.entityenumchecksqlgen;

import jakarta.persistence.AttributeConverter;

@SuppressWarnings("rawtypes")
class EnumAttributeConverter {

	private final Class<? extends AttributeConverter> converter;
	private final Class<?> attributeType;
	private final Class<?> databaseColumnType;

	EnumAttributeConverter(final Class<? extends AttributeConverter> converter, final Class<?> attributeType, final Class<?> databaseColumnType) {
		this.converter = converter;
		this.attributeType = attributeType;
		this.databaseColumnType = databaseColumnType;
	}

	public Class<?> getAttributeType() {
		return this.attributeType;
	}

	public Class<? extends AttributeConverter> getConverter() {
		return this.converter;
	}

	boolean isNumericColumn() {
		return Number.class.isAssignableFrom(this.databaseColumnType);
	}

	AttributeConverter<?, ?> newInstance() throws ReflectiveOperationException {
		return this.converter.getConstructor().newInstance();
	}

	@Override
	public String toString() {
		return this.converter.toString();
	}
}