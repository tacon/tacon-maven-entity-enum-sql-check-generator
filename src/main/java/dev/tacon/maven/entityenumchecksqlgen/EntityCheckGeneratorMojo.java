package dev.tacon.maven.entityenumchecksqlgen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Converter;
import jakarta.persistence.Entity;
import jakarta.persistence.MapKeyColumn;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

@SuppressWarnings({ "resource", "rawtypes", "unchecked" })
@Mojo(name = "generate", requiresDependencyResolution = ResolutionScope.COMPILE, defaultPhase = LifecyclePhase.COMPILE)
public class EntityCheckGeneratorMojo extends AbstractMojo {

	@Parameter(required = true)
	private File outputFile;

	@Parameter
	private boolean omitHeader;

	@Parameter
	private boolean sortEnumConstants;

	@Parameter
	private boolean sortEnumKeys;

	@Parameter
	private Set<String> exclusions;

	@Component
	private MavenProject project;

	private Reflections reflections;

	private Map<Class<?>, EnumAttributeConverter> enumAttrConverters;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			final Set<URL> urls = new HashSet<>();
			for (final String cp : this.project.getCompileClasspathElements()) {
				this.getLog().info(cp);
				urls.add(new File(cp).toURI().toURL());
			}
			final ClassLoader contextClassLoader = URLClassLoader.newInstance(
					urls.toArray(new URL[0]),
					Thread.currentThread().getContextClassLoader());

			Thread.currentThread().setContextClassLoader(contextClassLoader);

			this.reflections = new Reflections(
					new ConfigurationBuilder()
							.setUrls(ClasspathHelper.forClassLoader(contextClassLoader)));

			final Set<Class<?>> entities = this.reflections.getTypesAnnotatedWith(Entity.class);
			if (entities.isEmpty()) {
				this.getLog().info("No entity found");
				return;
			}

			if (this.exclusions == null) {
				this.exclusions = Collections.emptySet();
			}

			this.enumAttrConverters = findAutoAppliedEnumAttributeConverters(this.reflections);
			if (this.getLog().isDebugEnabled()) {
				this.getLog().debug("Found " + this.enumAttrConverters.size() + " converters " + this.enumAttrConverters);
			}

			this.exportPipeSeparated(entities);

		} catch (final DependencyResolutionRequiredException ex) {
			throw new MojoExecutionException("Dependency resolution failed", ex);
		} catch (final MalformedURLException ex) {
			throw new MojoExecutionException("Invalid classpath", ex);
		} catch (final FileNotFoundException ex) {
			throw new MojoExecutionException("Cannot open output file", ex);
		} catch (final IOException ex) {
			throw new MojoExecutionException("Cannot write to output file", ex);
		}
	}

	private void exportPipeSeparated(final Set<Class<?>> entities) throws FileNotFoundException, IOException {
		try (final FileOutputStream fos = new FileOutputStream(this.outputFile);
				final OutputStreamWriter osw = new OutputStreamWriter(fos);
				final BufferedWriter bw = new BufferedWriter(osw)) {

			if (!this.omitHeader) {
				bw.append("# schema|table|column|field|in_statement");
				bw.newLine();
			}

			for (final Class<?> entity : entities) {
				if (!this.exclusions.contains(entity.getName())) {
					new EntityClassProcessor(
							entity,
							this.exclusions,
							this.enumAttrConverters,
							this.sortEnumConstants, this.sortEnumKeys,
							this.reflections, this.getLog()).process(bw);
				}
			}
		}
	}

	private static Map<Class<?>, EnumAttributeConverter> findAutoAppliedEnumAttributeConverters(final Reflections reflections) {
		final Map<Class<?>, EnumAttributeConverter> enumAttrConverters = new HashMap<>();
		for (final Class<?> converterClass : reflections.getTypesAnnotatedWith(Converter.class)) {
			final Converter converter = converterClass.getAnnotation(Converter.class);
			if (converter.autoApply()) {
				final EnumAttributeConverter enumAttrConverter = enumAttributeConverter(converterClass);
				if (enumAttrConverter != null) {
					enumAttrConverters.put(enumAttrConverter.getAttributeType(), enumAttrConverter);
				}
			}
		}
		return enumAttrConverters;
	}

	static EnumAttributeConverter enumAttributeConverter(final Class converterClass) {

		for (final Type type : converterClass.getGenericInterfaces()) {
			if (type instanceof ParameterizedType) {
				final ParameterizedType parameterizedType = (ParameterizedType) type;
				if (AttributeConverter.class == parameterizedType.getRawType()) {
					final Type[] typeArguments = parameterizedType.getActualTypeArguments();
					if (typeArguments.length > 1) {
						final Type attrTypeArgument = typeArguments[0];
						final Class<?> attrType;
						if (attrTypeArgument instanceof ParameterizedType) {
							attrType = (Class<?>) ((ParameterizedType) attrTypeArgument).getRawType();
						} else {
							attrType = (Class<?>) attrTypeArgument;
						}
						if (attrType.isEnum()) {
							final Type dbColTypeArgument = typeArguments[1];
							final Class<?> dbColType;
							if (dbColTypeArgument instanceof ParameterizedType) {
								dbColType = (Class<?>) ((ParameterizedType) dbColTypeArgument).getRawType();
							} else {
								dbColType = (Class<?>) dbColTypeArgument;
							}
							return new EnumAttributeConverter(converterClass, attrType, dbColType);
						}
					}
				}
			}
		}
		return null;
	}

	static String columnName(final Field field, final boolean isMapKey) {
		if (isMapKey) {
			final MapKeyColumn column = field.getAnnotation(MapKeyColumn.class);
			return column == null || column.name().isEmpty() ? field.getName() : column.name();
		}
		final Column column = field.getAnnotation(Column.class);
		return column == null || column.name().isEmpty() ? field.getName() : column.name();
	}

	static Class<?> extractEnumParameterType(final Field field) {
		final Type fieldGenericType = field.getGenericType();
		if (fieldGenericType instanceof ParameterizedType) {
			final ParameterizedType paramType = (ParameterizedType) fieldGenericType;
			final Class<?> fieldType1 = (Class<?>) paramType.getActualTypeArguments()[0];
			if (fieldType1.isEnum()) {
				return fieldType1;
			}
		}
		return null;
	}
}
