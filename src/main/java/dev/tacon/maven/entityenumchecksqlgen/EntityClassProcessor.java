package dev.tacon.maven.entityenumchecksqlgen;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Convert;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Embedded;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.MapKeyEnumerated;
import jakarta.persistence.Table;

import org.apache.maven.plugin.logging.Log;
import org.reflections.Reflections;

class EntityClassProcessor {

	private final Class<?> entity;
	private final Set<String> exclusions;
	private final Map<Class<?>, EnumAttributeConverter> enumAttrConverters;
	private final boolean sortEnumConstants;
	private final boolean sortEnumKeys;
	private final Reflections reflections;
	private final Log log;

	private String discriminatorColumnName;

	EntityClassProcessor(
			final Class<?> entity,
			final Set<String> exclusions,
			final Map<Class<?>, EnumAttributeConverter> enumAttrConverters,
			final boolean sortEnumConstants, final boolean sortEnumKeys,
			final Reflections reflections, final Log log) {
		this.entity = entity;
		this.exclusions = exclusions;
		this.enumAttrConverters = enumAttrConverters;
		this.sortEnumConstants = sortEnumConstants;
		this.sortEnumKeys = sortEnumKeys;
		this.reflections = reflections;
		this.log = log;
	}

	void process(final BufferedWriter bw) throws IOException {

		final String entityName = this.entity.getName();

		final Table table = this.entity.getAnnotation(Table.class);
		final String tableName = table == null || table.name().isEmpty() ? this.entity.getSimpleName() : table.name();
		final String schemaName = table == null ? "" : table.schema();
		if (this.log.isDebugEnabled()) {
			this.log.debug("Entity=" + this.entity.getSimpleName() + ", schema=" + schemaName + ", table=" + tableName);
		}

		// inheritance

		this.discriminatorColumnName = this.appendDiscriminatorValues(bw, tableName, schemaName);

		// fields

		for (final Field field : this.entity.getDeclaredFields()) {
			if (!this.exclusions.contains(entityName + "." + field.getName()) && !Modifier.isTransient(field.getModifiers())) {
				final Class<?> fieldType = field.getType();
				if (Collection.class.isAssignableFrom(fieldType) || Map.class.isAssignableFrom(fieldType)) {
					final Class<?> maybeEnumType = EntityCheckGeneratorMojo.extractEnumParameterType(field);
					if (maybeEnumType != null) {
						final CollectionTable collectionTable = field.getAnnotation(CollectionTable.class);
						// TODO handle missing @CollectionTable annotation. see specifications for default behaviour
						if (collectionTable != null) {
							final String collectionTableName = collectionTable.name().isEmpty() ? tableName + "_" + field.getName() : collectionTable.name();
							final String collectionSchemaName = collectionTable.schema();
							final boolean isMapKey = Map.class.isAssignableFrom(fieldType);
							this.appendEnumType(bw, collectionTableName, collectionSchemaName, field, maybeEnumType, isMapKey);
						}
					}
				} else if (fieldType.isEnum()) {
					this.appendEnumType(bw, tableName, schemaName, field, fieldType, false);
				} else if (field.getAnnotation(EmbeddedId.class) != null || field.getAnnotation(Embedded.class) != null) {
					// TODO handle enum collections
					for (final Field embeddableField : fieldType.getDeclaredFields()) {
						final Class<?> embeddableFieldType = embeddableField.getType();
						if (embeddableFieldType.isEnum()) {
							this.appendEnumType(bw, tableName, schemaName, embeddableField, embeddableFieldType, false);
						}
					}
				}
			}
		}
	}

	// returns the discriminator column name
	private String appendDiscriminatorValues(final BufferedWriter bw, final String tableName, final String schemaName) throws IOException {
		final DiscriminatorColumn discriminatorColumn = this.entity.getAnnotation(DiscriminatorColumn.class);
		if (discriminatorColumn != null) {

			Collector<CharSequence, ?, String> joiningCollector = null;
			switch (discriminatorColumn.discriminatorType()) {
				case CHAR:
				case STRING:
					joiningCollector = Collectors.joining("','", "('", "')");
					break;
				case INTEGER:
					joiningCollector = Collectors.joining(",", "(", ")");
					break;
			}

			if (joiningCollector != null) {
				final String columnName = discriminatorColumn.name();
				bw.append(schemaName).append("|").append(tableName).append("|").append(columnName).append("|").append(columnName).append("|")
						.append(Stream.concat(Stream.of(this.entity), this.reflections.getSubTypesOf(this.entity).stream())
								.map(subclass -> subclass.getAnnotation(DiscriminatorValue.class))
								.filter(Objects::nonNull)
								.map(DiscriminatorValue::value)
								.sorted()
								.collect(joiningCollector));
				bw.newLine();

				return columnName;
			}
		}
		return null;
	}

	private void appendEnumType(final BufferedWriter bw, final String tableName, final String schemaName, final Field field, final Class<?> enumType, final boolean isMapKey) throws IOException {
		final String columnName = EntityCheckGeneratorMojo.columnName(field, isMapKey);
		if (columnName.equals(this.discriminatorColumnName)) {
			// already processed
			return;
		}
		this.log.debug("       Field=" + field.getName() + ", column=" + columnName);
		final Object[] enumConstants = enumType.getEnumConstants();
		bw.append(schemaName).append("|").append(tableName).append("|").append(columnName).append("|").append(field.getName()).append("|");

		final Convert convert = field.getAnnotation(Convert.class);
		if (convert != null) {
			this.applyConverter(bw, enumConstants, EntityCheckGeneratorMojo.enumAttributeConverter(convert.converter()));
		} else {
			final EnumAttributeConverter enumAttrConverter = this.enumAttrConverters.get(enumType);
			if (enumAttrConverter != null) {
				this.applyConverter(bw, enumConstants, enumAttrConverter);
			} else {
				boolean ordinal = false;
				if (isMapKey) {
					// looking for @MapKeyEnumerated annotation for map keys
					final MapKeyEnumerated enumerated = field.getAnnotation(MapKeyEnumerated.class);
					if (enumerated == null || EnumType.ORDINAL == enumerated.value()) {
						ordinal = true;
						bw.append(Stream.of(enumConstants)
								.map(Enum.class::cast)
								.map(Enum::ordinal)
								.map(String::valueOf)
								.collect(Collectors.joining(",", "(", ")")));
					}
				} else {
					final Enumerated enumerated = field.getAnnotation(Enumerated.class);
					if (enumerated == null || EnumType.ORDINAL == enumerated.value()) {
						ordinal = true;
						bw.append(Stream.of(enumConstants)
								.map(Enum.class::cast)
								.map(Enum::ordinal)
								.map(String::valueOf)
								.collect(Collectors.joining(",", "(", ")")));
					}
				}
				if (!ordinal) {
					// use enum constants names
					bw.append(this.sortEnumConstants(Stream.of(enumConstants)
							.map(Enum.class::cast)
							.map(Enum::name))
							.collect(Collectors.joining("','", "('", "')")));
				}
			}
		}
		bw.newLine();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void applyConverter(final BufferedWriter bw, final Object[] enumConstants, final EnumAttributeConverter enumAttrConverter) throws IOException {
		try {
			final AttributeConverter converterInstance = enumAttrConverter.newInstance();
			final List<Object> convertedValues = Stream.of(enumConstants).map(converterInstance::convertToDatabaseColumn).collect(Collectors.toList());
			final Collector<CharSequence, ?, String> collector = enumAttrConverter.isNumericColumn() ? Collectors.joining(",", "(", ")") : Collectors.joining("','", "('", "')");
			bw.append(this.sortEnumKeys(convertedValues.stream()
					.map(String::valueOf))
					.collect(collector));
		} catch (final ReflectiveOperationException ex) {
			this.log.error("Cannot convert enum constraint with converter: " + enumAttrConverter.getConverter(), ex);
		}
	}

	private Stream<String> sortEnumConstants(final Stream<String> stream) {
		return this.sortEnumConstants ? stream.sorted() : stream;
	}

	private Stream<String> sortEnumKeys(final Stream<String> stream) {
		return this.sortEnumKeys ? stream.sorted() : stream;
	}
}